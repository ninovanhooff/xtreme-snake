package com.ninovanhooff.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;
import com.ninovanhooff.snake.SnakeBodyParts;
import com.ninovanhooff.snake.controller.SnakeController;
import com.ninovanhooff.snake.resource.ResourceManager;

import static com.badlogic.gdx.math.MathUtils.random;

/**
 * Created by nino on 2/9/14.
 */
public class SnakeModel  extends Model{

    SnakeBodyParts bodyParts;
    ObjectMap<String,Sprite> sprites;
    final int startLength = 4;
    private BoardModel boardModel;
    DIRECTION direction;

    public SnakeModel(SnakeController controller,BoardModel boardModel) throws Exception {
        super(controller);
        //load snakeModel sprites
        sprites = new ObjectMap<String, Sprite>();
        String spritePath = ResourceManager.getAvailableSnakeSprite();
        TextureAtlas spriteSheet = new TextureAtlas(Gdx.files.internal(spritePath));
        for(String key : new String[]{"snake_body","snake_head"}){
            sprites.put(key, spriteSheet.createSprite(key));
        }

        this.boardModel = boardModel;

        bodyParts = new SnakeBodyParts(this);
        BoardSpace[][] spaces = boardModel.spaces;
        //pick a random starting spot, And see if it is available.
        int i=0,startX,startY;
        boolean validStartPos,horizontal,forward;
        do{
            validStartPos = true;
            //start within center half of the boardModel
            startX = random(boardModel.width / 4, 3 * (boardModel.width / 4));
            startY = random(boardModel.height / 4, 3 * (boardModel.height / 4));
            //from starting point, which direction will the snake
            //travel when ame starts?
            horizontal = random(1)==0?false:true;
            forward = random(1)==0?false:true;

            if(horizontal){
                if(forward){
                    direction = DIRECTION.RIGHT;
                    for(int x = startX;x<startX+startLength;x++){
                        if(spaces[x][startY].isOccupied()){
                            validStartPos = false;
                            break;
                        }
                    }
                }else{//horizontal backwards = to the left
                    direction = DIRECTION.LEFT;
                    for(int x = startX;x>startX-startLength;x--){
                        if(spaces[x][startY].isOccupied()){
                            validStartPos = false;
                            break;
                        }
                    }
                }
            }else{//vertical
                if(forward){//up
                    direction = DIRECTION.UP;
                    for(int y = startY;y<startY+startLength;y++){
                        if(spaces[startX][y].isOccupied()){
                            validStartPos = false;
                            break;
                        }
                    }
                }else{//vertical backwards = downwards
                    direction = DIRECTION.DOWN;
                    for(int y = startY;y>startY-startLength;y--){
                        if(spaces[startX][y].isOccupied()){
                            validStartPos = false;
                            break;
                        }
                    }
                }
            }
            i++;
        }while (i<1000 && !validStartPos);

        if(!validStartPos){
            //TODO: internationalization
            throw new Exception("could not fit all Snakes in playing field, try increasing the size");
        }else{
            // actually add the body parts
            if(horizontal){
                if(forward){
                    for(int x = startX;x<startX+startLength;x++){
                        bodyParts.addHead(x, startY);
                    }
                }else{//horizontal backwards = to the left
                    for(int x = startX;x>startX-startLength;x--){
                        bodyParts.addHead(x, startY);
                    }
                }
            }else{//vertical
                if(forward){//up
                    for(int y = startY;y<startY+startLength;y++){
                        bodyParts.addHead(startX, y);
                    }
                }else{//vertical backwards = downwards
                    for(int y = startY;y>startY-startLength;y--){
                        bodyParts.addHead(startX, y);
                    }
                }
            }
        }
    }

    public BoardModel getBoardModel() {
        return boardModel;
    }

    public SnakeBodyParts getBodyParts() {
        return bodyParts;
    }

    public ObjectMap<String, Sprite> getSprites() {
        return sprites;
    }


    public DIRECTION getDirection() {
        return direction;
    }

    public void setDirection(DIRECTION direction) {
        this.direction = direction;
    }
}
