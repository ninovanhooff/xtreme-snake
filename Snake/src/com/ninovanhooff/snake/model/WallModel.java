package com.ninovanhooff.snake.model;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ObjectMap;
import com.ninovanhooff.snake.controller.ConsumableController;
import com.ninovanhooff.snake.controller.Hitable;
import com.ninovanhooff.snake.controller.WallController;

import java.util.ArrayList;

/**
 * Created by nino on 2/14/14.
 */
public class WallModel {
    int x,y;
    WallController controller;
    ObjectMap<String,Sprite> sprites;

    //TODO: pool this or something? Prevent duplicates for apples etc?

    public WallModel(WallController controller, int x,int y) {
        this.controller = controller;
        this.x = x;
        this.y=y;
    }

    public WallController getController() {
        return controller;
    }

    public ObjectMap<String, Sprite> getSprites() {
        return sprites;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Sprite getSprite(String key) {
        return sprites.get(key);
    }
}
