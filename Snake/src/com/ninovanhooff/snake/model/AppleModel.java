package com.ninovanhooff.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ninovanhooff.snake.controller.AppleController;
import com.ninovanhooff.snake.controller.ConsumableController;
import com.ninovanhooff.snake.controller.Controller;

/**
 * Created by nino on 2/14/14.
 */
public class AppleModel extends ConsumableModel {

    /*public AppleModel(int xPos, int yPos) {
        super(xPos, yPos);

    }*/

    public AppleModel(AppleController controller,BoardModel boardModel) {
        super((ConsumableController)controller,boardModel);
        Texture texture  = new Texture(Gdx.files.internal("data/apple.png"));
        sprite = new Sprite(texture);
    }
}
