package com.ninovanhooff.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.ninovanhooff.snake.controller.AppleController;
import com.ninovanhooff.snake.controller.ConsumableController;
import com.ninovanhooff.snake.controller.Hitable;

import java.util.ArrayList;

/**
 * Created by nino on 2/14/14.
 */
public class ConsumableModel {
    int x,y;
    ConsumableController controller;
    Sprite sprite;
    //TODO: pool this or something? Prevent duplicates for apples etc?

    public ConsumableModel(ConsumableController controller, BoardModel boardModel) {
        this.controller = controller;
        BoardSpace[][] spaces = boardModel.getSpaces();
        //get all free spaces
        ArrayList<BoardSpace> freeSpaces = new ArrayList<BoardSpace>();
        for(int x=0;x<spaces.length;x++){
            BoardSpace[] column = spaces[x];
            for(int y=0;y<column.length;y++){
                if(!column[y].isOccupied()){//not occupied
                    freeSpaces.add(column[y]);
                }
            }
        }

        //take a random one
        BoardSpace position = freeSpaces.get(MathUtils.random(0, freeSpaces.size() - 1));
        position.addOccupant((Hitable)controller);
        x = position.getX();y= position.getY();
    }

    public ConsumableController getController() {
        return controller;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
