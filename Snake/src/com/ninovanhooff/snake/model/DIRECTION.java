package com.ninovanhooff.snake.model;

/**
 * Created by nino on 2/11/14.
 */
public enum DIRECTION {
    UP (1,0,1),
    DOWN(-1,0,-1),
    LEFT(-2,-1,0),
    RIGHT(2,1,0);

    private int numVal;
    private int xComponent;
    private int yComponent;

    DIRECTION(int numVal,int xComponent,int yComponent) {
        this.numVal = numVal;
        this.xComponent = xComponent;
        this.yComponent = yComponent;
    }

    public int xComponent(){return xComponent;}
    public int yComponent(){return yComponent;}


    public int numVal() {
        return numVal;
    }

    /** "Reverse lookup" from component values to DIRECTION
     *
     * @param x the xComponent
     * @param y the yComponent
     * @return the matching DIRECTION or null
     */
    public static DIRECTION getDirection(int x,int y){
        for (DIRECTION dir : DIRECTION.values()){
            if(dir.xComponent == x && dir.yComponent == y){
                return dir;
            }
        }
        return null;
    }
}
