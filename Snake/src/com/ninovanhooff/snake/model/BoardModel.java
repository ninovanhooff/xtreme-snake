package com.ninovanhooff.snake.model;

import com.ninovanhooff.snake.controller.BoardController;

/**
 * Created by nino on 2/9/14.
 */
public class BoardModel {
    int width, height;


    /**
     * An entry [x][y] is true when occupied  (by snake,wall,whatever)
     */
    BoardController controller;
    BoardSpace[][] spaces;

    public BoardModel(BoardController controller, int width, int height) throws Exception {
        this.controller = controller;
        if (width < 5 || height < 5)
            throw new Exception("Width and height of board must be at least 5");
        this.width = width;
        this.height = height;


        //create empty board
        spaces = new BoardSpace[width][height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                BoardSpace boardSpace = new BoardSpace(controller, x, y);
                spaces[x][y] = boardSpace;
            }
        }
    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public BoardSpace[][] getSpaces() {
        return spaces;
    }

    /*public ObjectMap<String, Sprite> getSprites() {
        return sprites;
    }*/
}
