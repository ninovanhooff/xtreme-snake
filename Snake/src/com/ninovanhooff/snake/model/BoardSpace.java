package com.ninovanhooff.snake.model;

import com.ninovanhooff.snake.controller.BoardController;
import com.ninovanhooff.snake.controller.Hitable;
import com.ninovanhooff.snake.controller.SnakeController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by nino on 2/15/14.
 */
public class BoardSpace {
    private final int x, y;
    final private ArrayList<Hitable> occupants = new ArrayList<Hitable>();
    final BoardController boardController;

    public BoardSpace(BoardController boardController,int x, int y) {
        this.boardController = boardController;
        this.x = x;
        this.y = y;
    }

    public void addOccupant(Hitable o) {
        synchronized (occupants) {
            occupants.add(o);
        }
    }
    //TODO replace by occupants.remove(Object)
    /**
     * Remove the object from the occupants
     *
     * @param o
     * @return true if removed, false if not found
     */
    public boolean removeOccupant(Object o) throws RuntimeException {
        int idx = occupants.indexOf(o);
        if (idx == -1) {
            throw new RuntimeException("Object not found");
            //Gdx.app.error(SnakeGame.TAG, "Object not found!",new Exception());
            //return false;
        } else {
            synchronized (occupants) {
                occupants.remove(idx);
            }
            return true;
        }
    }

    /**
     * true when this space has at least one occupant
     */
    public boolean isOccupied() {
        return occupants.size() > 0;
    }

    public void hitOccupants(SnakeController snakeController) {
        CopyOnWriteArrayList<Hitable> occupantsSnapshot = new CopyOnWriteArrayList<Hitable>(occupants);
        ArrayList<Hitable> removals = new ArrayList<Hitable>();
        Iterator<Hitable> i = occupantsSnapshot.iterator();
        while (i.hasNext()) {
            Hitable hitable = i.next();
            boolean remove = hitable.hit(snakeController);
            if (remove) {//returns true if it should be deleted
                removals.add(hitable);
            }
        }

        for(Hitable hitable: removals){
            occupants.remove(hitable);
            boardController.removeHitable(hitable);
        }

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
