package com.ninovanhooff.snake.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;
import com.ninovanhooff.snake.controller.AppleController;
import com.ninovanhooff.snake.controller.ConsumableController;
import com.ninovanhooff.snake.controller.Hitable;
import com.ninovanhooff.snake.controller.SnakeController;
import com.ninovanhooff.snake.controller.SolidWallController;
import com.ninovanhooff.snake.controller.WallController;

/**
 * Created by nino on 2/14/14.
 */
public class SolidWallModel extends WallModel implements Hitable {

    public SolidWallModel(SolidWallController controller,int x,int y) {
        super((WallController)controller,x,y);

        //load boardModel sprites
        sprites = new ObjectMap<String, Sprite>();
        TextureAtlas spriteSheet = new TextureAtlas(Gdx.files.internal("data/board.atlas"));
        for(String key : new String[]{"board_wall"}){
            Sprite sprite = spriteSheet.createSprite(key);
            sprites.put(key, sprite);
        }

    }

    @Override
    public boolean hit(SnakeController snakeController) {
        snakeController.die();
        return false;
    }
}
