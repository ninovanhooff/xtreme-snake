package com.ninovanhooff.snake.model;

import com.ninovanhooff.snake.controller.Controller;
import com.ninovanhooff.snake.controller.Hitable;
import com.ninovanhooff.snake.controller.SnakeController;

/**
 * Created by nino on 2/17/14.
 */
public class Model {
    Hitable controller;

    public Model(Hitable controller) {
        this.controller = controller;
    }

    public Hitable getController() {
        return controller;
    }
}
