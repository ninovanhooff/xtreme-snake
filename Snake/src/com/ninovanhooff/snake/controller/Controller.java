package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by nino on 2/11/14.
 */
public interface Controller {

    public void draw(Batch batch);

    /**
     * Perform action,probably every frame
     *
     * @param delta
     * @return true is this controller is done and should be deleted. False otherwise
     */
    boolean act(float delta);
}
