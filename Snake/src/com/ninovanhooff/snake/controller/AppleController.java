package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.ninovanhooff.snake.model.AppleModel;
import com.ninovanhooff.snake.model.SnakeModel;
import com.ninovanhooff.snake.view.ConsumableView;

/**
 * Created by nino on 2/14/14.
 */
public class AppleController extends ConsumableController implements Controller, Hitable {
    AppleModel model;
    ConsumableView view;
    BoardController boardController;

    /**
     * Create the consumable at a random unoccupied spot on the board.
     * Model and view are also created
     *
     * @param boardController
     */
    public AppleController(BoardController boardController) {
        this.boardController = boardController;
        model = new AppleModel(this, boardController.boardModel);
        view = new ConsumableView(model, boardController.boardView);
    }

    /**
     * Add an extra body part at the head of the snake, elongating it
     */
    public boolean hit(SnakeController snakeController) {
        SnakeModel snakeModel = snakeController.getModel();
        snakeModel.getBodyParts().addTail(model.getX(), model.getY());
        boardController.addHitable(new AppleController(boardController));
        return true;
    }

    @Override
    public void draw(Batch batch) {
        view.draw(batch);
    }

    @Override
    public boolean act(float delta) {
        return false;
    }
}
