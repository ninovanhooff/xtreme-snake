package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.mappings.Ouya;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.ninovanhooff.snake.SnakeBodyParts;
import com.ninovanhooff.snake.info.ActorInfo;
import com.ninovanhooff.snake.model.BoardSpace;
import com.ninovanhooff.snake.model.DIRECTION;
import com.ninovanhooff.snake.model.SnakeModel;
import com.ninovanhooff.snake.view.SnakeView;

import java.util.HashMap;

import static com.ninovanhooff.snake.SnakeGame.TAG;

/**
 * Created by nino on 2/11/14.
 */
public class SnakeController extends InputListener implements Controller, Hitable {
    SnakeModel model;
    SnakeView view;
    /**
     * Time in seconds since last move
     */
    float accumulator = 0.0f;

    /**
     * The regular time interval between moves (seconds)
     */
    float timeStep;

    /**
     * The last direction the player indicated he wants to change to in coming timestep
     * may be null if no buttons pressed since last timestep
     */
    DIRECTION nextDirection = null;
    boolean isDead = false;//default:false;

    public SnakeController(BoardController boardController, ActorInfo info) throws Exception {
        this.model = new SnakeModel(this, boardController.boardModel);
        this.view = new SnakeView(model, boardController.boardView);
        timeStep = info.getTimeStep();
    }

    /**
     * React to input from a controller
     */
    public void listenToController(com.badlogic.gdx.controllers.Controller controller) {
        controller.addListener(new OuyaControllerListener());
        //TODO: never unregisterd
    }

    //@Override
    public void draw(Batch batch) {
        view.draw(batch);
    }

    @Override
    public boolean act(float delta) {
        if (isDead) {
            return true;
        }
        accumulator += delta;
        if (accumulator >= timeStep) {//move the snake in the current direction at each timestep
            accumulator = 0f;
            if (nextDirection != null) {
                model.setDirection(nextDirection);
                nextDirection = null;
            }
            SnakeBodyParts body = model.getBodyParts();
            int[] head = body.getHead();
            DIRECTION direction = model.getDirection();
            int nextX = head[0] + direction.xComponent();
            int nextY = head[1] + direction.yComponent();
            BoardSpace nextBoardSpace = model.getBoardModel().getSpaces()[nextX][nextY];
            if (nextBoardSpace.isOccupied()) {//see if there is something ahead
                nextBoardSpace.hitOccupants(this);
            }
            body.addHead(nextX, nextY);
            body.removeTail();

        }
        return false;
    }

    public void die() {
        isDead = true;
    }

    public SnakeModel getModel() {
        return model;
    }

    public SnakeView getView() {
        return view;
    }

    public boolean isDead() {
        return isDead;
    }

    @Override
    public boolean hit(SnakeController snakeController) {
        snakeController.die();//the other snake hits us, he dies. Can also be ourselves
        return false;//we live on
    }

    class OuyaControllerListener extends ControllerAdapter {
        @Override
        public boolean buttonDown(com.badlogic.gdx.controllers.Controller controller, int buttonCode) {
            Gdx.app.log(TAG, "code " + buttonCode);
            if (buttonCode == Ouya.BUTTON_A) {//change direction to RIGHT
                Gdx.app.log(TAG, "button A pressed");
                inputRight();
            } else if (buttonCode == Ouya.BUTTON_Y) {//change direction to UP
                Gdx.app.log(TAG, "button Y pressed");
                inputUp();
            } else if (buttonCode == Ouya.BUTTON_U) {//change direction to left
                Gdx.app.log(TAG, "button U pressed");
                inputLeft();
            } else if (buttonCode == Ouya.BUTTON_O) {//change direction to down
                Gdx.app.log(TAG, "button O pressed");
                inputDown();
            } else {
                //input not consumed
                return false;
            }
            return true;
        }
    }

    void inputUp(){
        if (model.getDirection() != DIRECTION.DOWN) {//cannot reverse
            nextDirection = DIRECTION.UP;
        }
    }

    void inputDown(){
        if (model.getDirection() != DIRECTION.UP) {//cannot reverse
            nextDirection = DIRECTION.DOWN;
        }
    }
    void  inputLeft(){
        if (model.getDirection() != DIRECTION.RIGHT) {//cannot reverse
            nextDirection = DIRECTION.LEFT;
        }
    }

    void inputRight(){
        if (model.getDirection() != DIRECTION.LEFT) {//cannot reverse
            nextDirection = DIRECTION.RIGHT;
        }
    }


    public class SnakeInputListener extends InputListener {
        /** Keyboard mapping from input methods to keycodes
         *
         */
        HashMap<String,Integer> keyMap;

        public SnakeInputListener(HashMap<String,Integer> keyMap){
            this.keyMap = keyMap;
        }

        @Override
        public boolean keyDown(InputEvent event, int keycode) {
            Gdx.app.log("Snake", "Snake key down:" + keycode);

            if (keycode == keyMap.get("DOWN")) {
                inputDown();

            } else if (keycode == keyMap.get("UP")) {
                inputUp();

            }else if(keycode == keyMap.get("LEFT")){
                inputLeft();

            } else if (keycode == keyMap.get("RIGHT")){
                inputRight();

            }else{
                //input not consumed
                return false;
            }
            return true;

        }

    }
}
