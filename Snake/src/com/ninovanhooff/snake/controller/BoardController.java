package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.ninovanhooff.snake.SnakeGame;
import com.ninovanhooff.snake.info.ActorInfo;
import com.ninovanhooff.snake.model.BoardModel;
import com.ninovanhooff.snake.model.BoardSpace;
import com.ninovanhooff.snake.model.SnakeModel;
import com.ninovanhooff.snake.view.BoardView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nino on 2/11/14.
 */
public class BoardController implements Controller {
    BoardModel boardModel;
    BoardView boardView;
    ArrayList<SnakeController> snakeControllers;
    ArrayList<Controller> hitables;
    ActorInfo actorInfo;

    public BoardController(ActorInfo actorInfo) throws Exception {
        boardModel = new BoardModel(this, 25, 15);
        boardView = new BoardView(boardModel, actorInfo, SnakeGame.BOARD_UNIT_SIZE);
        this.actorInfo = actorInfo;

        snakeControllers = new ArrayList<SnakeController>();
        hitables = new ArrayList<Controller>();

        createOuterwall();
    }

    private void createOuterwall() {
        BoardSpace[][] spaces = boardModel.getSpaces();
        int width = boardModel.getWidth();
        int height = boardModel.getHeight();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                //edges are walls, make them true, others false (= traverseable)
                boolean hasWall = (x == 0 || y == 0 || x == width - 1 || y == height - 1);
                BoardSpace boardSpace = new BoardSpace(this, x, y);
                if (hasWall) {
                    SolidWallController solidWallController = new SolidWallController(this, x, y);
                    addHitable(solidWallController);
                    spaces[x][y].addOccupant(solidWallController);
                }
            }
        }
    }

    ;

    public void addSnake(com.badlogic.gdx.controllers.Controller controller) {

        try {
            SnakeController snakeController = new SnakeController(this, actorInfo);
            if (controller != null) {
                snakeController.listenToController(controller);

            }
            snakeControllers.add(snakeController);
        } catch (Exception e) {
            //TODO handle this
            e.printStackTrace();
        }

        //hitables.add(new AppleController(this));


    }

    public InputListener addSnake(HashMap<String, Integer> keymap){
        try {
            SnakeController snakeController = new SnakeController(this, actorInfo);
            snakeControllers.add(snakeController);
            return snakeController.new SnakeInputListener(keymap);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Draws the board and everything on it (eg. snakes, obstacles
     */
    public void draw(Batch batch) {
        //draw consumables and walls and maybe more
        for (Controller hitable : hitables) {
            hitable.draw(batch);
        }

        //draw snakes
        for (SnakeController snakeController : snakeControllers) {
            snakeController.draw(batch);
        }


    }

    @Override
    public boolean act(float delta) {
        boolean allDone = true;
        for (SnakeController snakeController : snakeControllers) {
            if (!snakeController.act(delta)) {
                allDone = false;
            }
        }
        return allDone; //is all snakes are dead, this game is over
    }

    public void addHitable(Controller hitable) {
        hitables.add(hitable);
    }

    public void removeHitable(Hitable hitable) {
        hitables.remove(hitable);
    }
}
