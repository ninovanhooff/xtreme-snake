package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.ninovanhooff.snake.model.WallModel;
import com.ninovanhooff.snake.view.WallView;

/**
 * Created by nino on 2/18/14.
 */
public class WallController implements Controller {
    WallView view;
    WallModel model;

    /*public WallController(BoardController boardController, int x,int y){
        model = new WallModel(this,x,y);
        view = new WallView(model,boardController.boardView);
    }*/

    @Override
    public void draw(Batch batch) {
        view.draw(batch);
    }

    @Override
    public boolean act(float delta) {
        return false;
    }
}
