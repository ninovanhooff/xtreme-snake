package com.ninovanhooff.snake.controller;

/**
 * Created by nino on 2/15/14.
 */
public interface Hitable {

    /**
     * Indicate that s snake has hit this object.
     *
     * @param snakeController
     * @return true if this object should be deleted afterwards (like an apple), false if it should live on (a wall)
     */
    boolean hit(SnakeController snakeController);
}
