package com.ninovanhooff.snake.controller;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.ninovanhooff.snake.model.SolidWallModel;
import com.ninovanhooff.snake.view.WallView;

/**
 * Created by nino on 2/14/14.
 */
public class SolidWallController extends WallController implements Hitable {

    BoardController boardController;

    /**
     * Create the consumable at a random unoccupied spot on the board.
     * Model and view are also created
     *
     * @param boardController
     */
    public SolidWallController(BoardController boardController, int x, int y) {
        this.boardController = boardController;
        model = new SolidWallModel(this, x, y);
        view = new WallView(model, boardController.boardView);
    }

    /**
     * Hit a solid wall and you die
     *
     * @param snakeController
     * @return false. Solid walls will survive
     */
    @Override
    public boolean hit(SnakeController snakeController) {
        snakeController.die();
        return false;
    }

    @Override
    public void draw(Batch batch) {
        view.draw(batch);
    }

    @Override
    public boolean act(float delta) {
        return false;
    }
}
