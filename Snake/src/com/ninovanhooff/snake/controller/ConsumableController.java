package com.ninovanhooff.snake.controller;

import com.ninovanhooff.snake.model.ConsumableModel;
import com.ninovanhooff.snake.view.ConsumableView;

/**
 * Created by nino on 2/14/14.
 */
public abstract class ConsumableController {
    ConsumableModel model;
    ConsumableView view;

    /**Create the consumable at a random unoccupied spot on the board.
     * Model and view are also created
     */
    //public ConsumableController(BoardModel boardModel){

    //}

    /**Let the snake "eat" the consumable. This method will not destroy the object.
     * It will just apply the effect(speed up, elongate) to the snake*/
    /*public abstract void consume(SnakeModel snake);

    public void draw(Batch batch){
        view.draw(batch);
    }
    */

}
