package com.ninovanhooff.snake.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ninovanhooff.snake.model.ConsumableModel;

/**
 * Created by nino on 2/14/14.
 */
public class ConsumableView implements Viewable {
    ConsumableModel consumableModel;
    BoardView boardView;
      public ConsumableView(ConsumableModel consumableModel, BoardView boardView){
          this.consumableModel = consumableModel;
          this.boardView = boardView;
          consumableModel.getSprite().setSize(boardView.boardUnitSize,boardView.boardUnitSize);
      }

    @Override
    public void draw(Batch batch) {
        Sprite sprite = consumableModel.getSprite();
        sprite.setPosition(boardView.boardXtoStageX(consumableModel.getX()),
                boardView.boardYtoStageY(consumableModel.getY()));
        sprite.draw(batch);
    }
}
