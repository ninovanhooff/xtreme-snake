package com.ninovanhooff.snake.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ninovanhooff.snake.model.SnakeModel;

import java.util.Vector;

/**
 * Created by nino on 2/11/14.
 */
public class SnakeView implements Viewable {
    SnakeModel snakeModel;
    BoardView boardView;

    public SnakeView(SnakeModel snakeModel,BoardView boardView){
        this.snakeModel = snakeModel;
        this.boardView = boardView;
        int boardUnitSize = boardView.boardUnitSize;
        //set sprite size
        for(Sprite sprite :snakeModel.getSprites().values()){
            sprite.setSize(boardUnitSize,boardUnitSize);
        }
    }
    public void draw(Batch batch){
        Vector<int[]> parts = snakeModel.getBodyParts().getParts();
        for(int i = 0;i<parts.size();i++){
            int[] part = parts.get(i);
            Sprite sprite;
            String spriteName;
            if(i==parts.size()-1){//snakeModel's head
                spriteName = "snake_head";
            }else{
                spriteName = "snake_body";
            }
            sprite = snakeModel.getSprites().get(spriteName);
            sprite.setPosition(boardView.boardXtoStageX(part[0]),
                    boardView.boardYtoStageY(part[1]));
            sprite.draw(batch);
            //batch.end();
            //batch.begin();
        }

    }
}
