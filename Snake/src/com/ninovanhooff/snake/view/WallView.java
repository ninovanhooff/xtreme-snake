package com.ninovanhooff.snake.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ninovanhooff.snake.model.WallModel;

/**
 * Created by nino on 2/14/14.
 */
public class WallView implements Viewable {
    WallModel wallModel;
    BoardView boardView;

    public WallView(WallModel wallModel, BoardView boardView) {
        this.wallModel = wallModel;
        this.boardView = boardView;

        //set sprite size
        int boardUnitSize = boardView.boardUnitSize;
        for (Sprite sprite : wallModel.getSprites().values()) {
            sprite.setSize(boardUnitSize, boardUnitSize);
        }

    }

    @Override
    public void draw(Batch batch) {
        Sprite sprite = wallModel.getSprite("board_wall");
        sprite.setPosition(boardView.boardXtoStageX(wallModel.getX()),
                boardView.boardYtoStageY(wallModel.getY()));
        sprite.draw(batch);
    }
}
