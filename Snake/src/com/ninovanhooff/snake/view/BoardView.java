package com.ninovanhooff.snake.view;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.ninovanhooff.snake.info.ActorInfo;
import com.ninovanhooff.snake.model.BoardModel;

/**
 * Created by nino on 2/11/14.
 */
public class BoardView  {
    BoardModel boardModel;

    /**The amount of pixels that one space of the boardModel takes in (both width and height)*/
    int boardUnitSize;

    /*Bottom left corner of boardModel walls in pixels from bottom left corner of Actor*/
    float boardOriginX,boardOriginY;

    /**The amount of in time in seconds between each snake move (in case of no power-ups) of this board*/
    float boardTimeStep;

    public BoardView(BoardModel boardModel, ActorInfo actorInfo, int boardUnitSize){
        this.boardModel = boardModel;
        this.boardUnitSize = boardUnitSize;
        boardTimeStep = actorInfo.getTimeStep();

        boardOriginX = (float) (actorInfo.getCenterX()-(boardModel.getWidth()/2.0)*boardUnitSize);
        boardOriginY = (float) (actorInfo.getCenterY()-(boardModel.getHeight()/2.0)*boardUnitSize);


    }

    /*public void draw(Batch batch){
        //draw boardModel outline
        Sprite wallSprite = boardModel.getSprites().get("board_wall");
        for(int x = 0;x< boardModel.getWidth();x++){
            for(int y=0;y< boardModel.getHeight();y++){
                //only draw outline (edges of boardModel)
                if(x==0 ||y == 0 ||x== boardModel.getWidth() -1||y== boardModel.getHeight() -1)
                    wallSprite.setPosition(boardXtoStageX(x),boardYtoStageY(y));
                wallSprite.draw(batch);
            }
        }
    }*/

    /**Convert board position to bottom-left X coordinate of Stage. Useful for drawing*/
    public float boardXtoStageX(int boardX){
        return boardOriginX+boardX*boardUnitSize;
    }

    /**Convert board position to bottom-left Y coordinate of Stage. Useful for drawing*/
    public float boardYtoStageY(int boardY){
        return boardOriginY+boardY*boardUnitSize;
    }
}
