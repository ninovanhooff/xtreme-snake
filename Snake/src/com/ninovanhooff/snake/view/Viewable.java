package com.ninovanhooff.snake.view;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by nino on 2/11/14.
 */
public interface Viewable {

    public void draw(Batch batch);
}
