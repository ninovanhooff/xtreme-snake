package com.ninovanhooff.snake;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.ninovanhooff.snake.resource.ResourceManager;

public class SnakeGame extends Game {
    public static final String TAG = "Snake";
    static final int gameW = 1280, gameH = 720;
    /**The amount of pixels that one space of the boardModel takes in (both width and height)*/
    public static final int BOARD_UNIT_SIZE = 48;

    private Stage stage;

	@Override
	public void create() {
        ResourceManager.initialize();
		final int screenW = Gdx.graphics.getWidth();
		final int screenH = Gdx.graphics.getHeight();

		stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        Group background = new Group();
        background.setBounds(0, 0, screenW, screenH);
        Group foreground = new Group();
        foreground.setBounds(0, 0, screenW, screenH);



        // Notice the order
        stage.addActor(background);
        stage.addActor(foreground);
        Actor gameActor;
        try

        {
            gameActor = new GameActor(gameW, gameH);
            stage.setKeyboardFocus(gameActor);
            foreground.addActor(gameActor);
        }

        catch(
        Exception e
        )

        {
            //TODO handle this
            e.printStackTrace();
        }
        // Or anything else you want to add like you normally would to the stage.

        //background.addActor(new Image()); // your background image here.


    }

    public void resize (int width, int height) {
        Vector2 size = Scaling.fit.apply(gameW,gameH, width, height);
        int viewportX = (int)(width - size.x) / 2;
        int viewportY = (int)(height - size.y) / 2;
        int viewportWidth = (int)size.x;
        int viewportHeight = (int)size.y;
        Gdx.gl.glViewport(viewportX, viewportY, viewportWidth, viewportHeight);
        stage.setViewport(gameW, gameH, true, viewportX, viewportY, viewportWidth, viewportHeight);
    }

    public void render () {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }


	@Override
	public void dispose() {

	}

	/*@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		sprite.draw(batch);
		batch.end();


        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(-0.0f,-0.0f,0.40f,0.4f);
        shapeRenderer.end();
	}*/

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
