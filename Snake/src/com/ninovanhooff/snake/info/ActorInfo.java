package com.ninovanhooff.snake.info;

import com.ninovanhooff.snake.GameActor;

/**
 * Created by nino on 2/11/14.
 */
public class ActorInfo {
    final private float width,height,centerX,centerY;
    final private float timeStep;

    public ActorInfo(GameActor actor){
        width = actor.getWidth();
        height = actor.getHeight();
        centerX = width/2;
        centerY = height/2;
        timeStep = actor.getTimeStep();
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getCenterX() {
        return centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    public float getTimeStep() {
        return timeStep;
    }
}
