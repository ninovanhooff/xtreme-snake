package com.ninovanhooff.snake.resource;

import java.util.HashMap;

/**
 * Created by nino on 6/30/14.
 */
public class KeyMapResource extends Resource {

    HashMap<String,Integer> keymap;

    public KeyMapResource(HashMap<String,Integer> keymap){
        this.keymap = keymap;
    }

    public HashMap<String, Integer> getKeymap() {
        return keymap;
    }


}
