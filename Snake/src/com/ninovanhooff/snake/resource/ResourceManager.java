package com.ninovanhooff.snake.resource;

import com.badlogic.gdx.Input;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by nino on 6/30/14.
 */
public class ResourceManager {
    static LinkedHashMap<String,KeyMapResource> keymaps;
    static LinkedHashMap<String,SnakeSpriteResource> snakeSprites;

    public static void initialize(){
        keymaps = new LinkedHashMap<String, KeyMapResource>();

        HashMap<String,Integer> player1KeyMap = new HashMap<String, Integer>();

        player1KeyMap.put("UP", Input.Keys.UP);
        player1KeyMap.put("DOWN",Input.Keys.DOWN);
        player1KeyMap.put("LEFT",Input.Keys.LEFT);
        player1KeyMap.put("RIGHT",Input.Keys.RIGHT);

        keymaps.put("Player 1",new KeyMapResource(player1KeyMap));

        HashMap<String,Integer> player2KeyMap = new HashMap<String, Integer>();

        player2KeyMap.put("UP", Input.Keys.W);
        player2KeyMap.put("DOWN",Input.Keys.S);
        player2KeyMap.put("LEFT",Input.Keys.A);
        player2KeyMap.put("RIGHT",Input.Keys.D);

        keymaps.put("Player 2",new KeyMapResource(player2KeyMap));

        snakeSprites = new LinkedHashMap<String, SnakeSpriteResource>();
        snakeSprites.put("Player 1",new SnakeSpriteResource("data/snake.atlas"));
        snakeSprites.put("Player 2",new SnakeSpriteResource("data/snake2.atlas"));
    }

    public static HashMap<String,Integer> getAvailableKeyMap(){
        return ((KeyMapResource)getAvailableResource(keymaps)).getKeymap();
    }

    private static Resource getAvailableResource(LinkedHashMap<String,? extends Resource> map){

        for(Resource resource : map.values()){
            if(resource.isAvailable){
                resource.isAvailable = false;
                return resource;
            }
        }
        throw new RuntimeException("No available resource");
    }

    public static String getAvailableSnakeSprite() {
        return ((SnakeSpriteResource)getAvailableResource(snakeSprites)).getPath();
    }
}
