package com.ninovanhooff.snake.resource;

/**
 * Created by nino on 7/1/14.
 */
public class SnakeSpriteResource extends Resource {
    String path;

    public SnakeSpriteResource(String path){
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
