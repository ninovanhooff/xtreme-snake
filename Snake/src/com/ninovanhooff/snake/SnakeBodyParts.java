package com.ninovanhooff.snake;

import com.ninovanhooff.snake.model.SnakeModel;

import java.util.Vector;

/**
 * Created by nino on 2/9/14.
 */
public class SnakeBodyParts {
    SnakeModel snakeModel;
    final Vector<int[]> parts;

    public SnakeBodyParts(SnakeModel snakeModel){
        this.snakeModel = snakeModel;
        parts = new Vector<int[]>();
    }
    /** Add bodypart to the end (head, front) of the snake*/
    public void addHead(int x, int y){
        parts.add(new int[]{x, y});
        snakeModel.getBoardModel().getSpaces()[x][y].addOccupant(snakeModel.getController());
    }
    /** Add bodypart to the start (tail, back) of the snake*/
    public void addTail(int x, int y) {
        parts.add(0,new int[]{x, y});
        snakeModel.getBoardModel().getSpaces()[x][y].addOccupant(snakeModel.getController());
    }
    public Vector<int[]> getParts() {
        return parts;
    }

    /**Remove first bodypart (tail) returns false when no part could be removed (empty snake)*/
    public boolean removeTail() {
        if(parts.size()<1){
            return  false;
        }
        int[] removed = parts.remove(0);
        snakeModel.getBoardModel().getSpaces()[removed[0]][removed[1]].removeOccupant(snakeModel.getController());
        return true;
    }

    public int[] getHead(){
        return parts.get(parts.size()-1);
    }


}
