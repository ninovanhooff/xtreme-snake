package com.ninovanhooff.snake;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.ninovanhooff.snake.controller.AppleController;
import com.ninovanhooff.snake.controller.BoardController;
import com.ninovanhooff.snake.info.ActorInfo;
import com.ninovanhooff.snake.resource.ResourceManager;

import java.util.HashMap;

/**
 * Created by nino on 2/9/14.
 */
public class GameActor extends Actor {
    boolean useKeyboard = true;
    TextureRegion region;
    final float centerX, centerY;//half width and height
    BoardController boardController;
    float accumulator = 0.0f;
    private float timeStep = 1.0f;
    //the duaration of one frame, which in snake is equal to
    //float frameTime = 1.0f/2;

    BitmapFont gameFont;

    public GameActor(int w,int h) throws Exception {
        setWidth(w);setHeight(h);
        //stageW = w; stageH = h;
        centerX = w/2;
        centerY = h/2;

        gameFont = new BitmapFont(Gdx.files.internal("data/block_font_32.fnt"),false);
        gameFont.setScale(3);

        boardController = new BoardController(new ActorInfo(this));


        if(useKeyboard){
            Gdx.app.log(SnakeGame.TAG, "Using keyboard");

            addListener(boardController.addSnake(ResourceManager.getAvailableKeyMap()));
            addListener(boardController.addSnake(ResourceManager.getAvailableKeyMap()));

        }else{
            for(Controller controller: Controllers.getControllers()) {
                Gdx.app.log(SnakeGame.TAG, controller.getName());
                boardController.addSnake(controller);
                break;//only one player for now
            }
        }


        boardController.addHitable(new AppleController(boardController));






        addAction(new Action() {
            @Override
            public boolean act(float delta) {
                boolean done;
                done = boardController.act(delta);
                if(done){

                }
                return done;
            }
        });

    }

       public void draw(Batch batch , float parentAlpha){
           //draw board with everything on it
           boardController.draw(batch);

           //draw score
           //gameFont.draw(batch,"0-2",50,50);
       }


    public float getTimeStep() {
        return timeStep;
    }
}


